from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}
    
class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    pass

@require_http_methods(["GET", "POST"])
def api_show_hats(request, id):
    pass


